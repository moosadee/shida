# Shida

![Shida Logo](shidalogo.png)

Reusable Javascript game boilerplate

View the template project here:
https://bitbucket.org/moosaderllc/shida-template-project

## Features:

* Sound/music manager
    * Volume control
* UI widget manager
    * Buttons
    * Labels
    * Images
* Language manager
    * Store and access strings for different languages
